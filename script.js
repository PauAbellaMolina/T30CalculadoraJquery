let num1 = 0;
let operator = null;
let num2 = 0;

$(document).ready(function(){
    $(".num").on("click",
        function(){
            updateLabel($(this).text());
        }
    );

    $(".operator").on("click",
        function(){
            operatorClicked($(this).text());
        }
    );

    $(".special").on("click",
        function(){
            specialClicked($(this).text());
        }
    );

    $(".equals").on("click",
        function(){
            equals();
        }
    );
});

//Al clicar un operador, almazena lo que hay en el label en la varible del primer numero y vacia el label
function operatorClicked(operatorParam) {
    num1 = Number($("#resultado").text());
    operator = operatorParam;
    setResult('');
}

//Al clicar el boton de =, mira que operacion tiene que hacer segun el operador, la calcula y printea en el label
function equals() {
    num2 = Number($("#resultado").text());
    switch(operator) {
        case '+':
            setResult(num1+num2);
            break;
        case '-':
            setResult(num1-num2);
            break;
        case '*':
            setResult(num1*num2);
            break;
        case '/':
            setResult(num1/num2);
            break;
        case '%':
            setResult(num1/100 * num2);
            break;
        case 'Elev':
            setResult(Math.pow(num1, num2));
            break;
    }
}

//Funcion que controla el . y anade este al numero del label; controla y realiza los CE y C
function specialClicked(action) {
    if(action == '.') {
        updateLabel('.');
    }
    if(action == 'CE') {
        $("#resultado").html('');
    }
    if(action == 'C') {
        num1 = 0;
        num2 = 0;
        $("#resultado").html('');
    }
}

//Funciones que reciben un texto o numero y lo printean en el label de resultado
function updateLabel(txt) {
    $("#resultado").append(txt);
}

function setResult(result) {
    $("#resultado").html(result);
}